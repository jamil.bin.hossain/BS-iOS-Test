//
//  MovieViewModel.swift
//  BS-iOS-Test
//
//  Created by Jamil Bin Hossain on 6/5/21.
//

import Foundation
import UIKit


protocol MovieViewModelDelegate {
    func showOrHideLoader(loader: Bool)
    func noInternetToast()
    func response(status: String, message: String)
    func successfulldataParse()
}

class MovieViewModel {
    
    var api_key: String? = "38e61227f85671163c275f9bd95a8803"
    
    var delegate: MovieViewModelDelegate?
    
    func hitLoginAction() {
        
        if Reachability.isConnectedToNetwork() {
            
            self.delegate?.showOrHideLoader(loader: true)
            
//            WebService().getData(resource: MovieDataRequest.create(viewModel: self)) { [weak self] (result) in
//                
//                switch result {
//                case .success(let result):
//                    if let result = result {
//                        //print("result : \(result)")
//                        
//                        self?.delegate?.successfulldataParse()
//                        
//                    }
//
//                case .failure(.badRequest):
//                    
//                    self?.delegate?.showOrHideLoader(loader: false)
//                    
//                case .failure(.domainError):
//                    self?.delegate?.showOrHideLoader(loader: false)
//                    print("error")
//                case .failure(.success):
//                    self?.delegate?.showOrHideLoader(loader: false)
//                    break
//                case .failure(.resourceNotFound):
//                    self?.delegate?.showOrHideLoader(loader: false)
//                    break
//                case .failure(.exist):
//                    self?.delegate?.showOrHideLoader(loader: false)
//                    break
//                case .failure(.serverError):
//                    self?.delegate?.showOrHideLoader(loader: false)
//                    self?.delegate?.response(status: "failed", message: "Opps! Something went wrong")
//                    break
//                case .failure(.decodingError):
//                    self?.delegate?.showOrHideLoader(loader: false)
//                    break
//                case .failure(.networkError):
//                    self?.delegate?.showOrHideLoader(loader: false)
//                    self?.delegate?.response(status: "failed", message: "Opps! Something went wrong")
//                    break
//                case .failure(.otherError):
//                    self?.delegate?.showOrHideLoader(loader: false)
//                    self?.delegate?.response(status: "failed", message: "Incorrect Username or Password")
//                    break
//                case .failure(.dataParsingFailed):
//                    self?.delegate?.showOrHideLoader(loader: false)
//                    break
//                }//
//            }

        } else {
            self.delegate?.noInternetToast()
        }
                
    }

}
