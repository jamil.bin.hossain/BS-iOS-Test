//
//  MovieListVC.swift
//  BS-iOS-Test
//
//  Created by Jamil Bin Hossain on 6/5/21.
//

import UIKit

class MovieListVC: UIViewController {

    var coordinator: MovieListVCCoordinator?
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}

extension MovieListVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 15
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieCell", for: indexPath) as! MovieListTVCell
        
        cell.movieTitle.text = "Hello Movie"
        cell.movieDetails.text = "hhdfshfkshjdfbsjhdfhsdf"
        
        return cell
    }
    
    
}
