//
//  MovieDataResponse.swift
//  BS-iOS-Test
//
//  Created by Jamil Bin Hossain on 6/5/21.
//

import Foundation

struct MovieDataResponse: Codable {
    let api_keys: String?
}
