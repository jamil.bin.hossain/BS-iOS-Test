//
//  MovieDataRequest.swift
//  BS-iOS-Test
//
//  Created by Jamil Bin Hossain on 6/5/21.
//

import Foundation

struct MovieDataRequest: Codable {
    let api_key: String
}

extension MovieDataRequest {
    static func create(viewModel: MovieViewModel) -> Resource {
        let movieRequest = MovieDataRequest(viewModel)
        
        let url = URL(string: URL.movieList)!
        guard let data = try? JSONEncoder().encode(movieRequest) else {
            fatalError("Error encoding order!")
        }
        var resource = Resource(url: url)
        //resource.httpMethod = HttpMethod.POST.rawValue
        //resource.parameter = data
        
        return resource
    }
}


extension MovieDataRequest {
    init?(_ viewModel: MovieViewModel) {
        guard let apiKey = viewModel.api_key else {
            return nil
        }
        
        self.api_key = apiKey
    }
}

