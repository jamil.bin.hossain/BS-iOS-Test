//
//  URL+Urls.swift
//  Tmmim
//
//  Created by Jamil Bin Hossain on 3/5/21.
//

import Foundation

extension URL{
    #if QA
    static let baseUrl  = "https://api.themoviedb.org"
    #else
    static let baseUrl  = "https://api.themoviedb.org"
    #endif
}

extension URL {

    static let movieList                               = "\(URL.baseUrl)/api/v1/auth/login/"
    
}
