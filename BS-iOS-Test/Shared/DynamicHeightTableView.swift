//
//  DynamicHeightTableView.swift
//  Raffek
//
//  Created by Asraful Alam on 12/3/21.
//

import Foundation
import UIKit

final class DynamicHeightTableView: UITableView {
    
    override var contentSize:CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
}
